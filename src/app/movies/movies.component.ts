import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})

export class MoviesComponent implements OnInit {

  name= "no movie";

  movies= [
    {"id": 1, "title": "Spider-Man: Into The Spider-Verse", "Studio": "Sony", "weekend_income": "35,400,000$"},
    {"id": 2, "title": "The Mule", "Studio": "WB", "weekend_income": "17,210,000$"},
    {"id": 3, "title": "Dr. Seuss' The Grinch(2018)", "Studio": "Uni.", "weekend_income": "11,580,00$"},
    {"id": 4, "title": "Ralph Breaks the Internet", "Studio": "BV", "weekend_income": "9,589,000$"},
    {"id": 5, "title": "Mortal Engines", "Studio": "Uni.", "weekend_income": "7,501,000$"}

    
  ];
 
  toDelete(element)
 {
   let x,y = this.movies;
   let z = element.id;

   if (this.movies.length == 1)
   {
     this.movies = [];
   }
   if (z > this.movies.length)
   {
     z = this.movies.length-1;
   }
   
   x = this.movies.slice(0,z-1);
   y = this.movies.slice(z, this.movies.length);
   this.movies = x;
   this.movies = this.movies.concat(y);
   this.name = element.title;
  
 }
 
  displayedColumns: string[] = ['id', 'title', 'Studio', 'weekend_income','delete'];
  
  constructor() { }

  ngOnInit() {
 
  }

}


